import subprocess
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--images', nargs='+')
parser.add_argument('-w', '--widths', nargs='+')
arg = parser.parse_args()

if arg.images is None or arg.widths is None:
   sys.exit("pholt.py -i [set of images] -w [set of widths]")
   
for i, width in enumerate(arg.widths):
   for image in arg.images:
      filename,ext = image.rsplit('.',1)
      path = filename + '-' + str(int(i)+1) + 'x.' + ext
      print("image name: " + image)
      print("new path: " +  path)
      subprocess.run(["cp", image, path])
      print(f'copied {image} to {path}')
      subprocess.run(["press", path, str(width + "x")])
      print(f'compressed {path}')
      
